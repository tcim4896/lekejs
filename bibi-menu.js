
RegisterService({
    name:"menu",
    open:{
        id:0,
        open:false
    },
    structure:[
        {
        text: "Op",
        event: {type:"open",path:0},
        list: [
            {
            text: "Mangler",
            event: {type:"route",path:"mangler"
            }
            },
            {
            text: "Langcheckup",
            event: {type:"route",path:"langcheckup"}
            },
            {
            text: "Dictionary",
            event: {type:"route",path:"dictionary"
            }
            },
            {
            text: "Recognite",
            event: {type:"route",path:"dictionary"
            }
            },
            {
            text: "Recognite",
            event: {type:"route",path:"dictionary"
            }
            },
                    {
            text: "Recognite",
            event: {type:"route",path:"dictionary"
            }
            },
                    {
            text: "Recognite",
            event: {type:"route",path:"dictionary"
            }
            },		      
        ],
    },
    {
        text: "Leke.js",
        event: {type:"open",path:1},
        list: [
            {
            text: "Quick start",
            event: {type:"route",path:"langcheckup"}
            },   
            {
            text: "API Reference",
            event: {type:"route",path:"mangler"
            }
            },
            {
            text: "Downloads",
            event: {type:"route",path:"leke/downloads"
            }
            },
        ]
        },
    {
        text: "Noteshare",
        event: {type:"open",path:2},
        list:[
            {
                text: "Quick start",
                event: {type:"route",path:"noteshare"}
            },
            {
                text: "Register",
                event: {type:"ref",path:"http://pinterest.com/"}
            },
        ]
    },
    ]
})

function Router(pageId){ // handler for request and routes
    switch(pageId){
        case "entrypoint":
            b(root,s("entrypoint"))
        break;
        case "langcheckup":
            b(root,s("langcheckup"))
        break;
        case "noteshare":
            b(root,s("noteshare"))
        break;
        case "dictea":
            b(root,s("dictionary"))
        break;
        case "mangler":
            b(root,s("mangler"))
        break;
        default:
            b(root,s("home"))
        break;
    }
}

RegisterService({
    name:Router,
    router:Router
})


// toggle menu
function toggle(id){
	for(let item of _.menu.structure){
		if(item.event.type=="open"){
			s("level"+item.event.path).style.height="calc(0%)";
		}
	}
	if(typeof id!=="undefined"){
		if(_.menu.open.id==id&&_.menu.open.open==true){
		_.menu.open={id,open:false};
			s("level"+id).style.height="0px";
		}else{
			_.menu.open={id,open:true};
			s("level"+id).style.height="auto";
		}
	}

}


function drawMenu(menuStructure){
	function menuAction(menuItem){
		switch(menuItem.event.type){
			case "open":
				toggle(menuItem.event.path);
			break;
			case "close":
				_.menu.open.open=false;
				toggle()
			break;
			case "route":
				Router(menuItem.event.path);// as a service // component missing correct
				_.menu.open.open=false;
				toggle();
			break;
			case "ref":
				window.open(menuItem.event.path)
			break;
			default:
				cl("No matching route..")
			break;
		}
	}

	b(root,o({id:"menu",class:"menu"}))

	for(let menuItem of menuStructure){// include huh?
		b(s("menu"),e(o({class:"item", text:menuItem.text}),"click",function(){		
			menuAction(menuItem)
		}))
		function drawLevel(menuItem,rows=2){
			const list=menuItem.list
			const len=list.length;
		
			const divs=Math.ceil(len/rows);
			let itemIndex=0;rows=3;
			if(menuItem.event.type=="open"){
				b(root,o({id:"level"+menuItem.event.path, class:"level", siblings:[
					// o({class:"bar", text:menuItem.text,siblings:[
					// 	e(o({class:"close-btn",text:"x"}),"mousedown",()=> menuAction(menuItem))
					// 	]})
					   ]})
				)
			}
			cl(divs,len,menuItem)
			for(let i=0;i<divs;i++){
				// create division
				b(s("level"+menuItem.event.path),o({id:"division"+i,class:"division"}))

				for(let x=0;x<rows;x++){
					let li=menuItem.list[itemIndex];
					if(typeof li=="object"){
						if(menuItem.event.type=="open"){
							// create level
							itemIndex++;
							b(s("division"+i),e(o({id:"level"+li.event.path,class:"item", text:li.text}),"click",function(){
								menuAction(li)
							}))					
						}else{
							itemIndex++;
							b(s("division"+i),e(o({class:"item", text:li.text}),"click",function(){
								menuAction(li)
							}))
						}						
					}else{
						b(s("division"+i),o({class:"empty",text:"hello"}))
					}
	
				}
			}

		}
		drawLevel(menuItem);
	}

	e(document.body,"mousedown",function(){
		if(_.menu.open.open==true){
			if(_.y<s("level"+_.menu.open.id).offsetTop){
				menuAction({event:{type:"close"}})
			}
		}

	})
}

drawMenu(_.menu.structure);