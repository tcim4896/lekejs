# Lekejs

## todo
> revert and make menu modular remove from package
> as well making components from other menu builds
> this package should only contain dom utilities

# Usage
> o
Create a new HTML Element

> e
Attach Event to HTML Element

> s(id)
Select element by id(mState)

> b
Bind HTML element to another

> input
Create new input field

> btn
Create a button

> text
Create a text node

> ee
function ee(...args){
  let elm,events;
  for(let arg of args){
    switch(typeof arg){
      case "object":
        elm=arg;
      break;
      case "string":
        events=arg.split(" ");
      break;
      case "function":
        cl(events)
        for(let event of events){
          elm.addEventListener(event,function(e){
            arg.call(e);
          })
        }
      break;
      default:
        console.log("Syntax error..")
      break;
    }
  }
  return elm;
}


